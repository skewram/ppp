// Tout en dans le nom
ArduinoMapFunction = (x, iMin, iMax, oMin, oMax) => {
  return ((x - iMin) * (oMax - oMin)) / (iMax - iMin) + oMin;
};

// Juste pour animé un truc de manière potentiellement qualitatif
EaseInOutQuadraticFunction = (t, b, c, d) => {
  t /= d / 2;
  if (t < 1) return (c / 2) * t * t + b;
  t--;
  return (-c / 2) * (t * (t - 2) - 1) + b;
};

// Fonction reproduisant un "clamp" (a titre personnel je trouve ce mot infâme) mathématique
function clamp(x, min, max) {
  return Math.min(Math.max(x, min), max);
}
