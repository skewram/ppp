(() => {
    window.onmousemove = (e) => {
        document.querySelectorAll("article p").forEach((el, i) => {
            el.style.boxShadow = -10*(e.clientX / el.getBoundingClientRect().x) + "px " + -5*(e.clientY / el.getBoundingClientRect().y) + "px 32px 10px rgba(0, 0, 0, 0.25)";
            console.log();
        });
    };
})();