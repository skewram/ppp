let letsgo = window.innerWidth > 960;
let amIScrollingOnTheFloor = false;
let isOnIndex = ["", "index.html"].includes((window.location.pathname).split("/").pop());
let lastScroll;

window.onresize = () => {
  letsgo = window.innerWidth > 960;
  // Remet les éléments à leurs place
  if(!letsgo){
    document.querySelectorAll("h1 span").forEach((el, i) => {
      el.style.transform = "none";
    });
    document.querySelector("#chrono").style.transform = "none";
    document.querySelector("#dev").style.transform = "none";
    document.querySelector("#chiffres").style.transform = "none";
    document.querySelector("#chiffres").style.opacity = 1;
  }
};
// Effet de parallax
document.onscroll = (e) => {
  // Toggle la fleche qui par nous tp en haut
  if(document.documentElement.scrollTop > window.innerHeight*0.1){
    document.querySelector(".gotop").style = "opacity: 1";
  }else{
    document.querySelector(".gotop").style = "opacity: 0";
  }
  // Switch la couleur de la navbar selon le scroll
  let switchNavbar = document.documentElement.scrollTop > (window.innerHeight - 56);
  if(switchNavbar && !document.querySelector("nav").classList.contains("alternate-navbar")){
    document.querySelector("nav").classList.add("alternate-navbar");
  }else if(!switchNavbar && document.querySelector("nav").classList.contains("alternate-navbar")){
    document.querySelector("nav").classList.remove("alternate-navbar");
  }
  if(switchNavbar && isOnIndex){
    document.getElementById("contexteNav").classList.add("active");
    document.getElementById("homeNav").classList.remove("active");
  }else if(!switchNavbar && isOnIndex){
    document.getElementById("contexteNav").classList.remove("active");
    document.getElementById("homeNav").classList.add("active");
  }

  if (letsgo) {
    let headerRatio = clamp(
      document.documentElement.scrollTop / window.innerHeight*0.8,
      0,
      1
    );
    if (![1, 0].includes(headerRatio)) {
      document.querySelectorAll("h1 span").forEach((el, i) => {
        el.style.transform = "translateY(" + 1000 * headerRatio + "%)";
      });
    }
    let chronoRatio = clamp(
      document.documentElement.scrollTop / (window.innerHeight * 0.8 * 2),
      0,
      1
    );
    //Parallax sur #chrono
    if (![1, 0].includes(chronoRatio)) {
      document.querySelector("#chrono").style.transform =
        "translateY(" + -20 * chronoRatio + "%)";
    }
    let devRatio = clamp(
      document.documentElement.scrollTop / (window.innerHeight * 0.8 * 2),
      0,
      1
    );
    //Parallax sur #dev
    if (![1, 0].includes(devRatio)) {
      document.querySelector("#dev").style.transform =
        "translateX(" +
        ArduinoMapFunction(-20 * devRatio, -20, 0, 0, -20) +
        "%)";
    }
    let chiffresRatio = clamp(
      document.documentElement.scrollTop / (window.innerHeight * 0.8 * 3),
      0,
      1
    );
    //Parallax sur #chiffres
    if (![1, 0].includes(chiffresRatio)) {
      document.querySelector("#chiffres").style.transform =
        "scale(" + chiffresRatio + ")";
      document.querySelector("#chiffres").style.opacity = ArduinoMapFunction(
        chiffresRatio,
        0,
        1,
        0.5,
        1
      );
    }
  }
};

function wowItScroll() {
  scrollTo({
    top: window.innerHeight,
    behavior: "smooth",
  });
}

function discordo() {
  scrollTo({
    top: document.getElementById("discordo").getBoundingClientRect().y - 56,
    behavior: "smooth",
  });
}

function twitchprime() {
  scrollTo({
    top: document.getElementById("twitchprime").getBoundingClientRect().y - 56,
    behavior: "smooth",
  });
}

function redsus() {
  scrollTo({
    top: document.getElementById("redsus").getBoundingClientRect().y - 56,
    behavior: "smooth",
  });
}

function startFromTheBottomNowWeHere() {
  scrollTo({
    top: 0,
    behavior: "smooth",
  });
}