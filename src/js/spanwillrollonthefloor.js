(() => {
  //Selectionne notre titre
  const myTitle = document.querySelector(".main h1");
  //Ajoute au début et à la fin de notre HTML contenu de le titre les balises <span></span> puis remplace les espaces par </span><span>
  myTitle.innerHTML = "<span>"
    .concat(document.querySelector(".main h1").textContent)
    .concat("</span>")
    .split(" ")
    .join("</span><span>");

  //Animation des <span>
  document.querySelectorAll("h1 span").forEach((el, i) => {
    el.animate(
      [
        {
          transform: "translateX(-100px)",
          opacity: 0,
        },
        {
          transform: "translateX(0px)",
          opacity: 1,
        },
      ],
      { duration: 1000 + 500 * i, easing: "ease-in-out" }
    );
  });
})();
