(() => {
  let x = 0.0;
  setInterval(() => {
    if (x.toFixed(2) == 3.14) x = 0;
    x += 0.01;
    y = Math.sin(x);
    document.querySelector("header.main").style.background =
      "radial-gradient(" +
      ArduinoMapFunction(
        EaseInOutQuadraticFunction(y, 0, 1, 1),
        0,
        1,
        25,
        100
      ) +
      "% " +
      ArduinoMapFunction(
        EaseInOutQuadraticFunction(y, 0, 1, 1),
        0,
        1,
        35,
        100
      ) +
      "% at 50% 125%, #AB0000 0%, #000000 100%";
  }, 25);
})();
